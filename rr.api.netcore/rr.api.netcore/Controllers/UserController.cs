﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using rr.api.netcore.Https.Authenticate;
using rr.api.netcore.Https.Responses;
using rr.api.netcore.Model.FirebaseDataAccess;
using rr.api.netcore.Services;
using rr.api.netcore.Utility;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace rr.api.netcore.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : RRCoreController
    {
      
        public override string ModuleName => "User";

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]AuthenticateRequest request)
        {
            this.MethodName = "authenticate";
            this.InputRequest = request;
            UserBL userBl = new UserBL();
            var response = userBl.Authenticate(request);
            return await ToActionResultAsync(response);
        }

        [AllowAnonymous]
        [HttpGet("getUserList")]
        public async Task<IActionResult> GetUserList()
        {
            MethodName = "getUserList";
            UserBL userBl = new UserBL();
            var response = userBl.GetUserList();
            return await ToActionResultAsync(response);
        }

        [HttpPost("getUserByUserId")]
        public async Task<IActionResult> GetUserByUserId([FromBody]GetUserByUserIdRequest request)
        {
            
            this.MethodName = "getUserByUserId";
            this.InputRequest = request;
            UserBL userBl = new UserBL();
            var response = userBl.GetUserByUserId(request);
            return await ToActionResultAsync(response);
        }


        [HttpPost("saveUser")]
        public async Task<IActionResult> SaveUser([FromBody]SaveUserRequest request)
        {
            this.MethodName = "getUserByUserId";
            this.InputRequest = request;
            UserBL userBl = new UserBL();
            userBl.UserAccess = this.UserAccess;
            var response = userBl.SaveUser(request);
            return await ToActionResultAsync(response);
        }

        [HttpPost("saveUserAndStaff")]
        public async Task<IActionResult> SaveUserAndStaff([FromBody]SaveUserAndStaffRequest request)
        {
            this.MethodName = "saveUserAndStaff";
            this.InputRequest = request;
            UserBL userBl = new UserBL();
            var response = userBl.SaveUserAndStaff(request);
            return await ToActionResultAsync(response);
        }

        [AllowAnonymous]
        [HttpPost("setperson")]
        public async Task<IActionResult> PostPerson(UserAccessModel model)
        {
            //FirebaseService firebaseService = new FirebaseService();
            // var result = await firebaseService.SetPersonAsync(value);
            return await ToActionResultAsync(new ApiResponse());
        }

        //// GET: api/<controller>
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

    }
}
