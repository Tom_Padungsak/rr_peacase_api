﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using rr.api.netcore.Https;
using rr.api.netcore.Https.Authenticate;
using rr.api.netcore.Https.Responses;

namespace rr.api.netcore.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        //[Authorize]
        //[Route("getuser")]
        //[HttpPost]
        //public async Task<IActionResult> GetUser([FromBody] InputRequest model)
        //{
        //    model = null;
        //    model.name.ToLower();
        //    //if (!ModelState.IsValid)
        //    //{
        //    //    return BadRequest(new ApiBadRequestResponse(400, ModelState));
        //    //}


        //    // SaveLogs();

        //    return Ok(new ApiOKResponse(200, model, new TokenAccessModel()));
        //}
    }
}
