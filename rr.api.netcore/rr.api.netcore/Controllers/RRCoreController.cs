﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using rr.api.netcore.Configurations;
using rr.api.netcore.Helpers.Enums;
using rr.api.netcore.Https.Authenticate;
using rr.api.netcore.Https.Responses;
using rr.api.netcore.Model.DataAccesses;
using rr.api.netcore.Utility;
using RRIServiceModel.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace rr.api.netcore.Controllers
{
    [AllowAnonymous]
    public abstract class RRCoreController : ControllerBase
    { 
        //private proproties
        private string methodName;
        private dynamic inputRequest;
        private TokenAccessModel token;
        private UserAccessModel userAccess;
        private bool enableSaveRequestBeforMapper;

        // abstract proproties
        public abstract string ModuleName { get; }
        public RRCoreController()
        {
        }

       
        public string RequestBody()
        {
            var bodyStream = new StreamReader(HttpContext.Request.Body);
            bodyStream.BaseStream.Seek(0, SeekOrigin.Begin);
            var bodyText = bodyStream.ReadToEnd();
            return bodyText;
        }

        //public proproties
        public UserAccessModel UserAccess
        {
            get {

                var cliamsIdentity = User?.Identity;
                string userAccessString = cliamsIdentity?.Name;
                if (!userAccessString.IsNullOrEmptyWhiteSpace())
                {
                    this.userAccess = JsonConvert.DeserializeObject<UserAccessModel>(userAccessString);
                }
                return this.userAccess;
            }
            set { this.userAccess = value; }
        }

        public TokenAccessModel TokenAccessModel
        {
            get { return this.token; }
            set { this.token = value; }
        }
      
        public string MethodName
        {
            get { return this.methodName; }
            set { this.methodName = value; }
        }
        public dynamic InputRequest
        {
            get { return this.inputRequest; }
            set { this.inputRequest = value; }
        }

        public bool EnableSaveRequestBeforMapper
        {
            get { return this.enableSaveRequestBeforMapper; }
            set { this.enableSaveRequestBeforMapper = value; }
        }

        //public virtual method

        public virtual IActionResult ToActionResult(ApiResponse result)
        {
            var _result = SetResult(result);
            //log...
            return _result;
        }

        public virtual async Task<IActionResult> ToActionResultAsync(ApiResponse result)
        {

            var _result = await SetResultAsync(result);

            LogApi logApi = new LogApi();

            if (this.enableSaveRequestBeforMapper)
            {
                var requestBody = JsonConvert.DeserializeObject<object>(RequestBody());
                if (HttpContext.Request.Method == "POST")
                    logApi.SaveLogObject(this.inputRequest, JObject.Parse(JsonConvert.SerializeObject(requestBody)), "SaveRequest", methodName);//save log output
            }

           
            logApi.SaveLogObject(result, this.InputRequest, this.ModuleName, this.methodName);



            return _result;
        }

        public virtual IActionResult ToTokenResult(ApiResponse result)
        {
            var _result = new ObjectResult(new ApiOKResponse(result.status.code, result.data, this.TokenAccessModel)) { StatusCode = (int)HandleStatusCode.MapBusinessStatusCodeToHttp(EnumManagement.ToEnum<BusinessStatusCode>(result.status.code.ToString())) };
            //log...
            return _result;
        }

        //private method
        private async Task<IActionResult> SetResultAsync(ApiResponse result)
        {
            return result.status.code switch
            {
                (int)BusinessStatusCode.Success => Ok(new ApiOKResponse(result.status.code, result.data, this.TokenAccessModel)),
                (int)BusinessStatusCode.CreationSuccessful => new ObjectResult(new ApiOKResponse(result.status.code, result.data, this.TokenAccessModel)) { StatusCode = (int)HandleStatusCode.MapBusinessStatusCodeToHttp(EnumManagement.ToEnum<BusinessStatusCode>(result.status.code.ToString())) },
                _ => new ObjectResult(new ApiExceptionResponse(result.status.code, result.data, this.TokenAccessModel)) { StatusCode = (int)HandleStatusCode.MapBusinessStatusCodeToHttp(EnumManagement.ToEnum<BusinessStatusCode>(result.status.code.ToString())) },
            };
        }

        private IActionResult SetResult(ApiResponse result)
        {
            return result.status.code switch
            {
                (int)BusinessStatusCode.Success => Ok(new ApiOKResponse(result.status.code, result.data, this.TokenAccessModel)),
                (int)BusinessStatusCode.CreationSuccessful => new ObjectResult(new ApiOKResponse(result.status.code, result.data, this.TokenAccessModel)) { StatusCode = (int)HandleStatusCode.MapBusinessStatusCodeToHttp(EnumManagement.ToEnum<BusinessStatusCode>(result.status.code.ToString())) },
                _ => new ObjectResult(new ApiExceptionResponse(result.status.code, result.data, this.TokenAccessModel)) { StatusCode = (int)HandleStatusCode.MapBusinessStatusCodeToHttp(EnumManagement.ToEnum<BusinessStatusCode>(result.status.code.ToString())) },
            };
        }
    }
}
