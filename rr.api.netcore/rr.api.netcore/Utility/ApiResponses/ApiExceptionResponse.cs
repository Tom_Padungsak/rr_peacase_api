﻿using rr.api.netcore.Https.Authenticate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Https.Responses
{
    public class ApiExceptionResponse : ApiResponse
    {
        public ApiExceptionResponse(int statusCode, Exception exception = null) : base(statusCode)
        {
            data = exception.GetBaseException();
        }

        public ApiExceptionResponse(int statusCode, Exception exception = null, TokenAccessModel dataToken = null) : base(statusCode)
        {
            data = exception.GetBaseException();
            token = dataToken;
        }

        public ApiExceptionResponse(int statusCode, dynamic exception = null, TokenAccessModel dataToken = null) : base(statusCode)
        {
            data = exception;
            token = dataToken;
        }
    }
}
