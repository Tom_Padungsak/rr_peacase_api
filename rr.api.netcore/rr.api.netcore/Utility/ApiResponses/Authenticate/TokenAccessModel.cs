﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Https.Authenticate
{
    public class TokenAccessModel
    {
        public string secretKey { get; set; }
        public string accessToken { get; set; }
        public string refreshToken { get; set; }
        public string tokenType { get; set; }
        public DateTime expiresIn { get; set; }
        public DateTime expiresAt { get; set; }

        public TokenAccessModel()
        {
            tokenType = "Bearer";
        }
    }
}
