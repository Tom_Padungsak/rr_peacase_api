﻿using rr.api.netcore.Https.Authenticate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Https.Responses
{
    public class ApiOKResponse : ApiResponse
    {
        public ApiOKResponse(int statusCode, object result, TokenAccessModel dataToken) :base (statusCode)
        {
            data = result;
            token = dataToken;
        }
    }
}
