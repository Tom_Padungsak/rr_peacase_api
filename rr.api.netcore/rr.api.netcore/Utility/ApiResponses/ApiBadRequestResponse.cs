﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using rr.api.netcore.Https.Authenticate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Https.Responses
{
    public class ApiBadRequestResponse : ApiResponse
    {
        public ApiBadRequestResponse(int statusCode, ModelStateDictionary modelState, TokenAccessModel dataToken = null) : base(statusCode)
        {
            if (modelState.IsValid)
            {
                throw new ArgumentException("ModelState must be invalid", nameof(modelState));
            }
            this.status = status;
            data = modelState.SelectMany(x => x.Value.Errors)
                .Select(x => x.ErrorMessage).ToArray();
            this.token = dataToken;
        }

        public ApiBadRequestResponse(int statusCode, dynamic exception, TokenAccessModel dataToken = null) : base(statusCode)
        {
           
            this.status = status;
            data = exception;
            this.token = dataToken;
        }
    }
}
