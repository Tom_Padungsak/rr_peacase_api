﻿using rr.api.netcore.Model.DataAccesses;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Https.Authenticate
{
    public class AuthenticateRequest
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }


    public class GetUserByUserIdRequest
    {
        [Required]
        public string userId { get; set; }

    }

    public class SaveUserRequest : MtUserDto
    {
    }

    public class SaveUserAndStaffRequest : MtUserDto
    {
        [Required]
        public string parentId { get; set; }

        public string staffTeamId { get; set; }
    }

    




}
