﻿using rr.api.netcore.Model.DataAccesses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Https.Response
{

    public class SaveUserResponse : MtUserDto
    {
        public string statusSave { get; set; }
    }
}
