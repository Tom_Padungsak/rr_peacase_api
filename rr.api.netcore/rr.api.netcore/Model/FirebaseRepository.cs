﻿using FireSharp;
using FireSharp.Config;
using FireSharp.EventStreaming;
using FireSharp.Interfaces;
using FireSharp.Response;
using rr.api.netcore.Https.Responses;
using rr.api.netcore.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Model
{
    public abstract class FirebaseRepository : IFirebaseClient, IFirebaseConfig
    {
        private IFirebaseConfig config;
        private IFirebaseClient client;
        private ApiResponse _apiResponse;
        public FirebaseRepository()
        {
            _apiResponse = new ApiResponse();
            config = new FirebaseConfig()
            {
                AuthSecret = "JVrUiZeIyBxMZ3Vv79HH4rbqcPmSka3KWRXmbi6U",
                BasePath = "https://cosmicstudiop1-8f867.firebaseio.com/"
            };

            client = new FirebaseClient(config);
        }

        public ApiResponse apiResponse => _apiResponse;
        public IFirebaseConfig Config { get => this.config; set => this.config = value; }
        public IFirebaseClient Client { get => this.client; set => this.client = value; }

        public string BasePath { get => config.BasePath; set => config.BasePath = value; }
        public string Host { get => config.Host; set => config.Host = value; }
        public string AuthSecret { get => config.AuthSecret; set => config.AuthSecret = value; }
        public TimeSpan? RequestTimeout { get => config.RequestTimeout; set => config.RequestTimeout = value; }
        public ISerializer Serializer { get => config.Serializer; set => config.Serializer = value; }

        public FirebaseResponse ChangeEmail(string oldEmail, string password, string newEmail)
        {
            return Client.ChangeEmail(oldEmail, password, newEmail);
        }

        public FirebaseResponse ChangePassword(string email, string oldPassword, string newPassword)
        {
            return Client.ChangePassword(email, oldPassword, newPassword);
        }

        public FirebaseResponse CreateUser(string email, string password)
        {
            return Client.CreateUser(email, password);
        }

        public FirebaseResponse Delete(string path)
        {
            return Client.Delete(path);
        }

        public async Task<FirebaseResponse> DeleteAsync(string path)
        {
            return await Client.DeleteAsync(path);
        }

        public FirebaseResponse Get(string path, QueryBuilder queryBuilder)
        {
            return Client.Get(path, queryBuilder);
        }

        public FirebaseResponse Get(string path)
        {
            return Client.Get(path);
        }

        public async Task<FirebaseResponse> GetAsync(string path)
        {
            return await Client.GetAsync(path);
        }

        public async Task<FirebaseResponse> GetAsync(string path, QueryBuilder queryBuilder)
        {
            return await Client.GetAsync(path, queryBuilder);
        }

        public async Task<EventStreamResponse> ListenAsync(string path, ValueAddedEventHandler added = null, ValueChangedEventHandler changed = null, ValueRemovedEventHandler removed = null)
        {
            return await Client.ListenAsync(path, added, changed, removed);
        }

        public async Task<EventStreamResponse> OnAsync(string path, ValueAddedEventHandler added = null, ValueChangedEventHandler changed = null, ValueRemovedEventHandler removed = null, object context = null)
        {
            return await Client.OnAsync(path, added, changed, removed, context);
        }

        public async Task<EventRootResponse<T>> OnChangeGetAsync<T>(string path, ValueRootAddedEventHandler<T> added = null)
        {
            return await Client.OnChangeGetAsync(path, added);
        }

        public PushResponse Push<T>(string path, T data)
        {
            return Client.Push(path, data);
        }

        public async Task<PushResponse> PushAsync<T>(string path, T data)
        {
            return await Client.PushAsync(path, data);
        }

        public FirebaseResponse RemoveUser(string email, string password)
        {
            return Client.RemoveUser(email, password);
        }

        public FirebaseResponse ResetPassword(string email, string password)
        {
            return Client.ResetPassword(email, password);
        }

        public SetResponse Set<T>(string path, T data)
        {
            return Client.Set(path, data);
        }

        public async Task<SetResponse> SetAsync<T>(string path, T data)
        {
            return await Client.SetAsync(path, data);
        }

        public FirebaseResponse Update<T>(string path, T data)
        {
            return Client.Update(path, data);
        }

        public async Task<FirebaseResponse> UpdateAsync<T>(string path, T data)
        {
            return await Client.UpdateAsync(path, data);
        }
    }
}
