﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Model.DataAccesses
{
    public class LogApi : AbRepository<LOG_API>
    {
        public override LOG_API GetById(string id)
        {
            throw new NotImplementedException();
        }

        public override List<LOG_API> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public bool SaveLogObject(object responseMessage, object requestMessage, string ModuleType, string ActionType)
        {
            string request = string.Empty;
            string response = JsonConvert.SerializeObject(responseMessage);
            JObject responseJson = JObject.Parse(response);

            if (requestMessage != null)
                request = JsonConvert.SerializeObject(requestMessage);


            var tran_id = responseJson.GetValue("transactionId") == null ? "" : responseJson.GetValue("transactionId").ToString();
            var req_datetime = responseJson.GetValue("transactionDateTime") != null ? Convert.ToDateTime(responseJson.GetValue("transactionDateTime").ToString()) : DateTime.Now;
            var status = responseJson.GetValue("code") != null ? responseJson.GetValue("code").ToString() : "";
            using (var db = this.DbContext)
            {
                LOG_API mylog = new LOG_API
                {
                    TRANSACTION_ID = tran_id,
                    ACTION_TYPE = ActionType,
                    MODULE_TYPE = ModuleType,
                    CREATE_BY = "",
                    CREATE_DATE = DateTime.Now,
                    REQUEST = request,
                    REQUEST_DATETIME = req_datetime,
                    RESPONSE = JsonConvert.SerializeObject(responseMessage),
                    RESPONSE_DATETIME = DateTime.Now,
                    STATUS = status
                };

                db.LOG_API.Add(mylog);
                db.SaveChanges();
            }
            return true;
        }
    }
}
