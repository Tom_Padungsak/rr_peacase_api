﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace rr.api.netcore.Helpers.Enums
{
    public enum BusinessStatusCode
    {
        [Description("Success")]
        Success = 1000,

        [Description("Creation successful")]
        CreationSuccessful = 1001,

        [Description("Data successfully updated ")]
        DataSuccessfullyUpdated = 1002,

        [Description("Missing required parameters")]
        MissingRequiredParameters = 1101,

        [Description("No Content/Data not found.")]
        NoContent = 2004,

        [Description("Authorization credentials required")]
        AuthorizationCredentialsRequired = 9100,

        [Description("Required standard headers (FIELD_NAME) not found")]
        RequiredStandardHeaders = 9101,

        [Description("Invalid/expired temporary token")]
        ExpiredToken = 9300,

        [Description("Invalid authorization credentials")]
        InvalidAuthorizationCredentials = 9500,

        [Description("Invalid access rights")]
        InvalidAccessRights = 9503,

        [Description("Generic server side error")]
        GenericServerSideError = 9700,

        [Description("API is not working time")]
        APIIsNotWorkingTime = 9800,

        [Description("Threat has been detected")]
        ThreatHasBeenDetected = 9900,
    }

    public static class HandleStatusCode
    {
        public static BusinessStatusCode MapStatusCodeHttpToBusiness(HttpStatusCode httpStatusCode)
        {
            return httpStatusCode switch
            {
                HttpStatusCode.OK => BusinessStatusCode.Success,
                HttpStatusCode.Created => BusinessStatusCode.CreationSuccessful,
                HttpStatusCode.BadRequest => BusinessStatusCode.MissingRequiredParameters,
                HttpStatusCode.Unauthorized => BusinessStatusCode.AuthorizationCredentialsRequired,
                HttpStatusCode.Forbidden => BusinessStatusCode.InvalidAccessRights,
                HttpStatusCode.NonAuthoritativeInformation => BusinessStatusCode.RequiredStandardHeaders,
                HttpStatusCode.InternalServerError => BusinessStatusCode.ThreatHasBeenDetected,
                HttpStatusCode.NoContent => BusinessStatusCode.NoContent,
                _ => throw new NotImplementedException(),
            };
        }

        public static HttpStatusCode MapBusinessStatusCodeToHttp(BusinessStatusCode BusinessHttpStatusCode)
        {
            return BusinessHttpStatusCode switch
            {
                BusinessStatusCode.Success => HttpStatusCode.OK,
                BusinessStatusCode.CreationSuccessful => HttpStatusCode.Created,
                BusinessStatusCode.MissingRequiredParameters => HttpStatusCode.BadRequest,
                BusinessStatusCode.AuthorizationCredentialsRequired => HttpStatusCode.Unauthorized,
                BusinessStatusCode.InvalidAccessRights => HttpStatusCode.Forbidden,
                BusinessStatusCode.RequiredStandardHeaders => HttpStatusCode.NonAuthoritativeInformation,
                BusinessStatusCode.ThreatHasBeenDetected => HttpStatusCode.InternalServerError,
                BusinessStatusCode.NoContent => HttpStatusCode.NoContent,
                _ => throw new NotImplementedException(),
            };
        }
    }
}
