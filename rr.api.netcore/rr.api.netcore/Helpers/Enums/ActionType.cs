﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Helpers.Enums
{
    public enum ActionType
    {
        [Description("Add")]
        [DefaultValue("Add")]
        Add = 0,

        [Description("Edit")]
        [DefaultValue("Edit")]
        Edit = 1,

        [Description("Delete")]
        [DefaultValue("Delete")]
        Delete = 2,

        [Description("Check")]
        [DefaultValue("Check")]
        Check = 4,

        [Description("Get")]
        [DefaultValue("Get")]
        Get = 5
    }
}
