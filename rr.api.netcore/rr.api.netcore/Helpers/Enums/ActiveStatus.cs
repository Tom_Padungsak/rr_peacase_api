﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Helpers.Enums
{
    public enum ActiveStatus
    {
        [Description("Yes")]
        [DefaultValue("Y")]
        Yes = 1,

        [Description("No")]
        [DefaultValue("N")]
        No = 0,

        [Description("Cancel")]
        [DefaultValue("C")]
        Cancel = -1,
    }
}
