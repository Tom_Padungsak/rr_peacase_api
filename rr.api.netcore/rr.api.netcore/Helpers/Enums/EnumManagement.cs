﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Helpers.Enums
{
    public static class EnumManagement
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string Description(this System.Enum source)
        {
            var enumType = source.GetType();
            var field = enumType.GetField(source.ToString());
            var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length == 0
                ? source.ToString()
                : ((DescriptionAttribute)attributes[0]).Description;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string Value(this System.Enum source)
        {
            var enumType = source.GetType();
            var field = enumType.GetField(source.ToString());
            var Value = field.GetCustomAttributes(typeof(DefaultValueAttribute), false);
            return Value.Length == 0
                ? source.ToString()
                : ((DefaultValueAttribute)Value[0]).Value.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ToEnum<T>(this string value)
        {
            return (T)System.Enum.Parse(typeof(T), value, true);
        }
    }
}
