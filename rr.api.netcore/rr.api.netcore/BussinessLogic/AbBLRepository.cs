﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using rr.api.netcore.Https.Authenticate;
using rr.api.netcore.Https.Responses;
using rr.api.netcore.IServices;

using RRIServiceModel.Models;

namespace rr.api.netcore.BussinessLogic
{
    public abstract class AbBLRepository : IService
    {

        private string moduleName;
        private ApiResponse apiResponse;
        private UserAccessModel userAccess;
        private AppModelDbContext dbContext;
        private IDbContextTransaction dbContextTransaction;

        public AbBLRepository()
        {
            //dbContext = _dbContext;
            apiResponse = new ApiResponse();
            var connectionString = Startup.Configuration.GetSection("AppSettings").GetConnectionString("AppDbContext");
            var optionsBuilder = new DbContextOptionsBuilder<AppModelDbContext>();
            optionsBuilder.UseNpgsql(connectionString);
            this.dbContext = new AppModelDbContext(optionsBuilder.Options);

        }

        protected string ModuleName
        {
            get { return this.moduleName = "AbBLRepository"; }
            set { this.moduleName = value; }
        }

        public UserAccessModel UserAccess
        {
            get { return this.userAccess; }
            set { this.userAccess = value; }
        }
        public ApiResponse ApiResponse => apiResponse;
        public AppModelDbContext DbContext
        {
            get { return this.dbContext; }
            set { this.dbContext = value; }
        }

        public void DbContextTransactionStart()
        {
            this.dbContextTransaction =  this.dbContext.Database.BeginTransaction(); 
            
        }
        public void DbContextTransactionCommit()
        {
            this.dbContextTransaction.Commit();
        }

        public void DbContextTransactionDispose()
        {
            this.dbContextTransaction.Dispose();
        }

        public void DbContextTransactionRollback()
        {
            this.dbContextTransaction.Rollback();
        }

        public void Dispose()
        {
            this.Dispose();
        }
    }
}
