﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Configurations
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public ConnectionString ConnectionString { get; set; }
        public string AzureStorageAccountContainer { get; set; }
        public DateFormatter DateFormat { get; set; }
        public NumberFormatter NumberFormat { get; set; }
    }

    public class ConnectionString
    {
        public string AppDbContext { get; set; }
    }
    public class DateFormatter
    {
        public string dateTimeFormat { get; set; }
        public string dateFormat { get; set; }
    }
    public class NumberFormatter
    {
        public string decimalFormat { get; set; }
    }
}
