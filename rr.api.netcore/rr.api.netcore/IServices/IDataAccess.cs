﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace rr.api.netcore.IServices
{
    public interface IDataAccess<T> where T : class, new()
    {
        abstract T GetById(string id);
        IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetAll();

        bool Create(T entity);
        bool Create(params T[] entities);
        bool Create(IEnumerable<T> entities);


        bool Delete(T entity);
        bool Delete(params T[] entities);

        bool Edit(T entity);
        bool Edit(params T[] entities);

    }
}
