﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using rr.api.netcore.BussinessLogic;
using rr.api.netcore.Configurations;
using rr.api.netcore.Helpers.Enums;
using rr.api.netcore.Https.Authenticate;
using rr.api.netcore.Https.Responses;
using rr.api.netcore.IServices;
using rr.api.netcore.Model.DataAccesses;
using rr.api.netcore.Security;
using RRIServiceModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;

namespace rr.api.netcore.Services
{
    public class UserService : AbBLRepository
    {
        private readonly AppSettings _appSettings;
        public JWTManagement jWTManagement;
        //public UserService(AppModelDbContext _dbContext, IOptions<AppSettings> appSettings) : base(_dbContext)
        //{
        //    _appSettings = appSettings.Value;
        //    jWTManagement = new JWTManagement(_appSettings);
        //}

        public ApiResponse Authenticate(AuthenticateRequest model)
        {
            MtUser mtUser = new MtUser();
            mtUser.Data = mtUser.Authenticate(model);
            if (mtUser.Data == null) return new ApiResponse() { status = new StatusResponse() { code = (int)BusinessStatusCode.InvalidAccessRights }, data = "Username or password is incorrect" };

            UserAccessModel user = new UserAccessModel()
            {
                fullName = mtUser.Data.NAME,
                firstName = mtUser.Data.NAME,
                lastName = "",
                password = mtUser.Data.PASSWORD,
                userId = mtUser.Data.USER_ID,
                username = mtUser.Data.USERNAME
            };

            var token = jWTManagement.GenerateJwtToken(JsonConvert.SerializeObject(user));
            ApiResponse.data = new AuthenticateResponse(user, token);
            return ApiResponse;
        }

        public ApiResponse GetUserList()
        {
            MtUser mtUser = new MtUser();
            mtUser.Datas = mtUser.GetAll();

            if (mtUser.Datas.ToList().Count > 0)
            {
                ApiResponse.data = mtUser.Datas;
            }
            else
            {
                ApiResponse.status.code = (int)BusinessStatusCode.NoContent;
            }
            return ApiResponse;
        }

    }
}
