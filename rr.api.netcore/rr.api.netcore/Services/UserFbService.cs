﻿using FireSharp;
using FireSharp.Config;
using FireSharp.Response;
using Newtonsoft.Json;
using rr.api.netcore.Https.Responses;
using rr.api.netcore.Model;
using rr.api.netcore.Model.FirebaseDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Services
{
    public class UserFbService : FirebaseRepository
    {
        public async Task<ApiResponse> SetPersonAsync(string path, Person _person)
        {
            SetResponse response = await Client.SetAsync(path, _person);
            apiResponse.data = response.ResultAs<Person>();
            return apiResponse;
        }

        public async Task<ApiResponse> GetPersonAsync(string path)
        {
            var response = await Client.GetAsync(path);
            apiResponse.data = response.ResultAs<Person>();
            return apiResponse;
        }

        public async Task<ApiResponse> GetPersonAsync(string path, QueryBuilder queryBuilder)
        {
            var response = await Client.GetAsync(path, queryBuilder);
            apiResponse.data = response.ResultAs<Person>();
            return apiResponse;
        }
    }
}
